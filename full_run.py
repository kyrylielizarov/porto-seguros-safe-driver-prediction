import pandas as pd
import numpy as np

from scipy.stats import randint
from sklearn.preprocessing import Imputer
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.pipeline import Pipeline

import matplotlib.pyplot as plt
import seaborn as sns

import sys
from time import localtime, strftime

def main():
    train = pd.read_csv('./data/train.csv', na_values=-1, index_col='id')

    #print(train.info())
    #print(train.head())

    test = pd.read_csv('./data/test.csv', na_values=-1, index_col='id')

    #print(test.info())
    #print(test.head())

    X = train.drop('target', axis=1).values
    y = train['target']
    X_tosubmit = test.values

    # Setup the pipeline steps: steps
    steps = [('imputation', Imputer(missing_values='NaN', strategy='median', axis=0)),
             ('LogReg', LogisticRegression(class_weight='balanced'))]

    # Specify the hyperparameter space
    parameters = {'SVM__C': [1, 10],
                  'SVM__gamma': [0.1, 0.01]}

    # Create the pipeline: pipeline
    pipeline = Pipeline(steps)

    # Create training and test sets
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

    # Instantiate the GridSearchCV object: cv
    # cv = GridSearchCV(pipeline, parameters, cv=3)

    print("Started fitting model at {0}".format(strftime("%Y-%m-%d %H:%M:%S", localtime())))
    # Fit to the training set
    pipeline.fit(X_train, y_train)
    print("Ended fitting model at {0}".format(strftime("%Y-%m-%d %H:%M:%S", localtime())))

    # Predict the labels of the test set
    y_pred = pipeline.predict(X_test)

    # Compute metrics
    # Compute and print metrics
    print("Accuracy: {}".format(pipeline.score(X_test, y_test)))
    print(classification_report(y_test, y_pred))
    # print("Tuned Model Parameters: {}".format(pipeline.best_params_))

    y_tosubmit = pipeline.predict_proba(X_tosubmit)

    print(y_tosubmit)
    print(pipeline.classes_)
    y_df = pd.DataFrame(y_tosubmit[:, 1], index=test.index, columns=['target'])
    print(y_df.head())
    y_df.to_csv('./submissions/submission_10102017.csv')

if __name__ == '__main__':
    main()
